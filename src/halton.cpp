#include "halton.h"

#undef NDEBUG
#include <cassert>
#include <cmath>

namespace {

struct RNGState {
    uint32_t seed;
};

inline uint32_t rand_lcg(RNGState &rng_state)
{
    rng_state.seed = 1664525 * rng_state.seed + 1013904223;
    return rng_state.seed;
}

void permute_in_place(std::vector<uint32_t> &values, RNGState &rng_state)
{
    const uint32_t num_values = values.size();
    for (uint32_t i = 0; i < num_values; ++i) {
        const uint32_t j = i + rand_lcg(rng_state) % (num_values - i);
        const auto tmp = values[i];
        values[i] = values[j];
        values[j] = tmp;
    }
}

// Returns the N first prime bases of the Halton sequence, starting from 2.
std::vector<uint32_t> get_halton_bases(const uint32_t num_bases)
{
    // Generating prime numbers is expensive, so we set a reasonable limit here on the maximum
    // number of bases that can be requested
    assert(num_bases < 200);

    auto bases = std::vector<uint32_t>{};
    bases.reserve(num_bases);
    uint32_t base_count = 0;
    uint32_t value = 2;
    while (base_count < num_bases) {
        bool is_prime = true;
        for (uint32_t i = 2; i < value; ++i) {
            if (value % i == 0) {
                is_prime = false;
                break;
            }
        }

        if (is_prime) {
            bases.push_back(value);
            base_count += 1;
        }

        value += 1;
    }

    return bases;
}

float halton(const uint32_t index, const uint32_t base, const std::vector<uint32_t> &perm)
{
    assert(perm.size() == base);

    float f = 1.0f;
    float result = 0.0f;
    uint32_t i = index;
    while (i > 0) {
        f = f / base;
        result += f * perm[i % base];
        i = uint32_t(std::floor(float(i) / base));
    }

    return result;
}

} // namespace

namespace ptvol {

// This function generates N digit-permuted Halton sequences with M samples per sequence. The
// permutation prevents correlation between higher bases, which is a well-known limitation of the
// original unpermuted Halton sequence. The sequences are returned as a flat array to simplify
// uploading to GPU memory.
std::vector<float> generate_permuted_halton_sequences(const uint32_t num_sequences,
                                                      const uint32_t num_samples,
                                                      const uint32_t seed)
{
    const std::vector<uint32_t> bases = get_halton_bases(num_sequences);

    RNGState rng_state = {seed};
    std::vector<float> halton_sequences(num_sequences * num_samples);
    for (uint32_t i = 0; i < num_sequences; ++i) {
        const uint32_t base = bases[i];

        // Generate permutation table for the current base
        std::vector<uint32_t> permutation_table;
        permutation_table.reserve(base);
        for (uint32_t digit = 0; digit < base; ++digit) {
            permutation_table.push_back(digit);
        }
        permute_in_place(permutation_table, rng_state);
        assert(permutation_table.size() == base);

        // Calculate permuted Halton sequence
        for (uint32_t j = 0; j < num_samples; ++j) {
            halton_sequences[i * num_samples + j] = halton(j, base, permutation_table);
        }
    }

    return halton_sequences;
}

} // namespace ptvol
