#include "file_dialog.h"

#include <imgui.h>
#include <tinydir.h>

#undef NDEBUG
#include <cassert>
#include <vector>

namespace {

std::vector<std::string> get_dirnames(const std::string &path)
{
    std::vector<std::string> dirnames = {};

    tinydir_dir dir;
    tinydir_open_sorted(&dir, path.c_str());
    for (uint32_t i = 0; i < dir.n_files; ++i) {
        tinydir_file file;
        tinydir_readfile_n(&dir, &file, i);
        if (!file.is_dir) {
            continue;
        }

        const std::string dirname = {file.name};
        if (dirname.empty() || dirname.at(0) == '.') {
            continue;
        }
        dirnames.push_back(dirname);
    }
    tinydir_close(&dir);

    return dirnames;
}

std::vector<std::string> get_filenames(const std::string &path)
{
    std::vector<std::string> filenames = {};

    tinydir_dir dir;
    tinydir_open_sorted(&dir, path.c_str());
    for (uint32_t i = 0; i < dir.n_files; ++i) {
        tinydir_file file;
        tinydir_readfile_n(&dir, &file, i);
        if (file.is_dir) {
            continue;
        }

        const std::string filename = {file.name};
        if (filename.empty() || filename.at(0) == '.') {
            continue;
        }
        filenames.push_back(filename);
    }
    tinydir_close(&dir);

    return filenames;
}

std::string join(const std::string &path, const std::string &file_or_dirname)
{
    assert(!path.empty());
    return path == "/" ? path + file_or_dirname : path + "/" + file_or_dirname;
}

std::string get_parent_dir_path(const std::string &path)
{
    assert(!path.empty());
    const size_t delimiter_pos = path.rfind('/');
    return delimiter_pos == 0 ? path.substr(0, 1) : path.substr(0, delimiter_pos);
}

std::vector<std::string> split(const std::string &path)
{
    assert(!path.empty());
    assert(path.at(0) == '/');

    if (path.size() == 1) {
        return {path};
    }

    std::vector<std::string> splitted_path = {};
    size_t delimiter_pos = path.find('/');
    while (delimiter_pos != std::string::npos) {
        const size_t next_delimiter_pos = path.find('/', delimiter_pos + 1);
        const size_t subpath_length = next_delimiter_pos != std::string::npos
                                          ? next_delimiter_pos - delimiter_pos
                                          : std::string::npos;
        splitted_path.push_back(path.substr(delimiter_pos, subpath_length));
        delimiter_pos = next_delimiter_pos;
    }

    return splitted_path;
}

} // namespace

namespace ptvol {

std::string file_dialog_open(FileDialogState &state)
{
    const std::vector<std::string> splitted_path = split(state.current_dir);
    const std::vector<std::string> dirnames = get_dirnames(state.current_dir);
    const std::vector<std::string> filenames = get_filenames(state.current_dir);

    ImGui::Text("Open file");
    ImGui::Separator();

    assert(!splitted_path.empty());
    for (size_t i = 0; i < splitted_path.size(); ++i) {
        if (i > 0) {
            ImGui::SameLine();
        }
        if (ImGui::SmallButton(splitted_path[i].c_str())) {
            state.current_dir = "";
            for (size_t j = 0; j <= i; ++j) {
                state.current_dir += splitted_path[j];
            }
        }
    }

    ImGui::BeginChild("dir_list", ImVec2(400, 200), true, ImGuiWindowFlags_HorizontalScrollbar);
    if (ImGui::Selectable("..")) {
        state.current_dir = get_parent_dir_path(state.current_dir);
        state.selected_filename = "";
    }
    for (const auto &dirname : dirnames) {
        if (ImGui::Selectable(("/" + dirname).c_str())) {
            state.current_dir = join(state.current_dir, dirname);
            state.selected_filename = "";
        }
    }
    for (const auto &filename : filenames) {
        const bool is_selected =
            !state.selected_filename.empty() && filename == state.selected_filename;
        if (ImGui::Selectable(filename.c_str(), is_selected)) {
            state.selected_filename = filename;
        }
    }
    ImGui::EndChild();

    if (ImGui::Button("Open") && !state.selected_filename.empty()) {
        ImGui::CloseCurrentPopup();
        return join(state.current_dir, state.selected_filename);
    }
    ImGui::SameLine();
    if (ImGui::Button("Cancel")) {
        ImGui::CloseCurrentPopup();
    }

    return "";
}

} // namespace ptvol
