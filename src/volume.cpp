#include "volume.h"

#include <glm/gtc/matrix_transform.hpp>

#undef NDEBUG
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <omp.h>

namespace {

inline int64_t clamp(const int64_t x, const int64_t min_val, const int64_t max_val)
{
    assert(min_val <= max_val);
    return x < min_val ? min_val : (x > max_val ? max_val : x);
}

void byteswap(std::vector<int16_t> &values)
{
    const size_t num_values = values.size();
    #pragma omp parallel for schedule(static)
    for (size_t i = 0; i < num_values; ++i) {
        const int16_t x = values[i];
        values[i] = ((x << 8) & 0xFF00) | ((x >> 8) & 0x00FF);
    }
}

// Converts int16 Hounsfield unit (HU) values to normalized uint8 values in the range [0,255]
void int16_to_uint8(const std::vector<int16_t> &int16_hu_values, std::vector<uint8_t> &uint8_values)
{
    const double min_hu = -1024.0;
    const double max_hu = 3071.0;

    const uint64_t num_values = int16_hu_values.size();
    uint8_values.resize(num_values);
    #pragma omp parallel for schedule(static)
    for (uint64_t i = 0; i < num_values; ++i) {
        const double hu_clamped = std::fmax(min_hu, std::fmin(max_hu, double(int16_hu_values[i])));
        const double value_01 = (hu_clamped - min_hu) / (max_hu - min_hu);
        uint8_values[i] = uint8_t(std::fmin(255.0, std::floor(256.0 * value_01)));
    }
}

} // namespace

namespace ptvol {

bool load_uint8_vtk_volume(const char *filename, VolumeUInt8 &volume)
{
    std::FILE *fp = std::fopen(filename, "rb");
    if (fp == nullptr) {
        std::fclose(fp);
        return false;
    }

    // Read header
    const uint32_t num_header_lines = 10;
    const uint32_t max_line_length = 256;
    char lines[num_header_lines][max_line_length];
    for (uint32_t i = 0; i < num_header_lines; ++i) {
        if (std::fgets(lines[i], max_line_length, fp) == nullptr) {
            std::fclose(fp);
            return false;
        }
    }

    if (!(std::strncmp(lines[0], "# vtk", 5) == 0 || std::strncmp(lines[0], "# VTK", 5) == 0)) {
        std::fclose(fp);
        return false;
    }
    if (!(std::strncmp(lines[2], "BINARY", 6) == 0)) {
        std::fclose(fp);
        return false;
    }
    if (!(std::strncmp(lines[3], "DATASET STRUCTURED_POINTS", 25) == 0)) {
        std::fclose(fp);
        return false;
    }
    uint32_t dx, dy, dz = 0;
    if (!(std::strncmp(lines[4], "DIMENSIONS", 10) == 0 &&
          std::sscanf(lines[4], "%*s %u %u %u", &dx, &dy, &dz) == 3)) {
        std::fclose(fp);
        return false;
    }
    float sx, sy, sz = 0.0f;
    // spacing and origin might be swapped, so we need to check both lines 5 and 6
    if (!((std::strncmp(lines[5], "SPACING", 7) == 0 &&
           std::sscanf(lines[5], "%*s %f %f %f", &sx, &sy, &sz) == 3) ||
          (std::strncmp(lines[6], "SPACING", 7) == 0 &&
           std::sscanf(lines[6], "%*s %f %f %f", &sx, &sy, &sz) == 3))) {
        std::fclose(fp);
        return false;
    }
    size_t num_voxels = 0;
    if (!(std::strncmp(lines[7], "POINT_DATA", 10) == 0 &&
          std::sscanf(lines[7], "%*s %lu", &num_voxels) == 1)) {
        std::fclose(fp);
        return false;
    }
    assert(size_t(dx) * dy * dz == num_voxels);
    char data_type[256];
    if (!(std::strncmp(lines[8], "SCALARS", 7) == 0 &&
          std::sscanf(lines[8], "%*s %*s %s", data_type) == 1)) {
        std::fclose(fp);
        return false;
    }
    if (!(std::strncmp(data_type, "unsigned_char", 13) == 0 ||
          std::strncmp(data_type, "short", 5) == 0)) {
        std::fclose(fp);
        return false;
    }

    volume.dimensions = glm::uvec3(dx, dy, dz);
    volume.spacing = glm::vec3(sx, sy, sz);

    // Read data
    volume.data.resize(num_voxels);
    if (std::strncmp(data_type, "unsigned_char", 13) == 0) {
        const size_t result = std::fread(volume.data.data(), sizeof(uint8_t), num_voxels, fp);
        std::fclose(fp);
        if (result != num_voxels) {
            return false;
        }
    }
    else if (std::strncmp(data_type, "short", 5) == 0) {
        std::vector<int16_t> int16_volume_data(num_voxels, 0);
        const size_t result = std::fread(int16_volume_data.data(), sizeof(int16_t), num_voxels, fp);
        std::fclose(fp);
        if (result != num_voxels) {
            return false;
        }
        byteswap(int16_volume_data);
        int16_to_uint8(int16_volume_data, volume.data);
    }

    return true;
}

glm::vec3 get_volume_extent(const VolumeUInt8 &volume)
{
    return glm::vec3(volume.dimensions) * volume.spacing;
}

// Returns a transform that scales a 2-unit cube centered at origin to the physical extent of the
// volume image
glm::mat4 get_volume_matrix(const VolumeUInt8 &volume)
{
    return glm::scale(glm::mat4(1.0f), 0.5f * get_volume_extent(volume));
}

void create_max_blocks(const VolumeUInt8 &volume, const glm::uvec3 &num_blocks,
                       VolumeUInt8 &max_blocks)
{
    assert(volume.dimensions[0] > 0);
    assert(volume.dimensions[1] > 0);
    assert(volume.dimensions[2] > 0);
    assert(num_blocks[0] > 0);
    assert(num_blocks[1] > 0);
    assert(num_blocks[2] > 0);

    const size_t width = volume.dimensions[0];
    const size_t height = volume.dimensions[1];
    const size_t depth = volume.dimensions[2];
    assert(width * height * depth == volume.data.size());

    const double block_width = double(width) / num_blocks[0];
    const double block_height = double(height) / num_blocks[1];
    const double block_depth = double(depth) / num_blocks[2];

    // The high-resolution source volume will typically be sampled with some form of interpolation
    // during rendering, so we set a small (one-voxel thick) margin for the max block computation to
    // avoid artifacts at block boundaries
    const int64_t margin = 1;

    max_blocks.dimensions = num_blocks;
    max_blocks.spacing = glm::vec3(block_width, block_height, block_depth) * volume.spacing;
    max_blocks.data.resize(num_blocks[0] * num_blocks[1] * num_blocks[2]);

    #pragma omp parallel for schedule(static)
    for (size_t k = 0; k < num_blocks[2]; ++k) {
        for (size_t j = 0; j < num_blocks[1]; ++j) {
            for (size_t i = 0; i < num_blocks[0]; ++i) {
                // Determine block bounds
                const size_t ii_min =
                    clamp(int64_t(std::floor(i * block_width)) - margin, 0, width - 1);
                const size_t ii_max =
                    clamp(int64_t(std::ceil((i + 1) * block_width)) + margin, 0, width);
                const size_t jj_min =
                    clamp(int64_t(std::floor(j * block_height)) - margin, 0, height - 1);
                const size_t jj_max =
                    clamp(int64_t(std::ceil((j + 1) * block_height)) + margin, 0, height);
                const size_t kk_min =
                    clamp(int64_t(std::floor(k * block_depth)) - margin, 0, depth - 1);
                const size_t kk_max =
                    clamp(int64_t(std::ceil((k + 1) * block_depth)) + margin, 0, depth);

                // Find maximum intensity value in block
                uint8_t max_intensity = 0;
                for (size_t kk = kk_min; kk < kk_max; ++kk) {
                    for (size_t jj = jj_min; jj < jj_max; ++jj) {
                        for (size_t ii = ii_min; ii < ii_max; ++ii) {
                            const size_t voxel_index = ii + jj * width + kk * width * height;
                            const uint8_t intensity = volume.data[voxel_index];
                            max_intensity = intensity > max_intensity ? intensity : max_intensity;
                        }
                    }
                }

                const size_t block_index =
                    i + j * num_blocks[0] + k * num_blocks[0] * num_blocks[1];
                max_blocks.data[block_index] = max_intensity;
            }
        }
    }
}

} // namespace ptvol
