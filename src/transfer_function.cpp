/// @file
/// @brief Transfer function implementation and utilities.
///
/// @section LICENSE
///
/// Copyright (C) 2016  Johan Nysjö
///
/// This software is distributed under the MIT license. See the
/// included LICENSE.txt file for details.

#include "transfer_function.h"

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <picojson.h>

#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <stdint.h>

namespace {

float linearstep(const float edge0, const float edge1, const float value)
{
    return glm::clamp((value - edge0) / (edge1 - edge0), 0.0f, 1.0f);
}

glm::vec3 srgb2rgb(const glm::vec3 &color)
{
    return glm::pow(color, glm::vec3(2.2f));
}

} // namespace

namespace ptvol {

// Loads color transfer function points from a JSON file.
bool transfer_function_load(TransferFunction &tf, const std::string &filename)
{
    // Read JSON file
    std::ifstream json_file(filename);
    picojson::value json;
    json_file >> json;

    const std::string err = picojson::get_last_error();
    if (!err.empty()) {
        return false;
    }

    auto json_obj = json.get<picojson::object>();

    // Extract material transfer function from JSON data
    {
        auto tf_point_array = json_obj["material_tf_points"].get<picojson::array>();
        for (const auto &tf_point : tf_point_array) {
            auto tf_point_json = tf_point.get<picojson::object>();

            // Extract position
            const double position = tf_point_json["position"].get<double>();

            // Extract material properties
            auto material_json = tf_point_json["material"].get<picojson::object>();
            auto albedo_json = material_json["albedo"].get<picojson::array>();
            const glm::vec3 albedo = {albedo_json[0].get<double>(), albedo_json[1].get<double>(),
                                      albedo_json[2].get<double>()};
            const double metallicness = material_json["metallicness"].get<double>();
            const double emissive = material_json["emissive"].get<double>();

            ptvol::MaterialTFPoint material_point;
            material_point.position = position;
            material_point.material.albedo = albedo;
            material_point.material.metallicness = metallicness;
            material_point.material.emissive = emissive;

            tf.material_points.push_back(material_point);
        }
    }

    // Extract opacity transfer function from JSON data
    {
        auto tf_point_array = json_obj["opacity_tf_points"].get<picojson::array>();
        for (const auto &tf_point : tf_point_array) {
            auto tf_point_json = tf_point.get<picojson::object>();

            const double position = tf_point_json["position"].get<double>();
            const double opacity = tf_point_json["opacity"].get<double>();

            ptvol::OpacityTFPoint opacity_point;
            opacity_point.position = position;
            opacity_point.opacity = opacity;

            tf.opacity_points.push_back(opacity_point);
        }
    }

    tf.is_dirty = false;

    return true;
}

// Creates a material and opacity transfer function texture from a set of TF points. Note: it is
// assumed that the TF points are sorted by position in increasing order.
void transfer_function_create_texture(TransferFunction &tf)
{
    // Allocate transfer function buffer
    const uint32_t width = 512;
    const uint32_t height = 2;
    const uint32_t num_channels = 4;
    std::vector<float> tf_buffer(width * height * num_channels, 0.0f);

    // Interpolate material properties between TF control points
    for (uint32_t i = 0; i < width; ++i) {
        // Compute 1D texture coordinate (intensity)
        const float intensity = float(i) / (width - 1);

        // Interpolate material properties
        const uint32_t num_points = tf.material_points.size();
        Material material;
        if (intensity <= tf.material_points[0].position) {
            material.albedo = srgb2rgb(tf.material_points[0].material.albedo);
            material.metallicness = tf.material_points[0].material.metallicness;
            material.emissive = tf.material_points[0].material.emissive;
        }
        else if (intensity >= tf.material_points[num_points - 1].position) {
            material.albedo = srgb2rgb(tf.material_points[num_points - 1].material.albedo);
            material.metallicness = tf.material_points[num_points - 1].material.metallicness;
            material.emissive = tf.material_points[num_points - 1].material.emissive;
        }
        else {
            uint32_t index = 0;
            for (uint32_t j = 0; j < num_points - 1; ++j) {
                if (intensity >= tf.material_points[j].position) {
                    index = j;
                }
            }

            const float mix_factor = linearstep(tf.material_points[index].position,
                                                tf.material_points[index + 1].position, intensity);
            material.albedo =
                glm::mix(srgb2rgb(tf.material_points[index].material.albedo),
                         srgb2rgb(tf.material_points[index + 1].material.albedo), mix_factor);
            material.metallicness =
                glm::mix(tf.material_points[index].material.metallicness,
                         tf.material_points[index + 1].material.metallicness, mix_factor);
            material.emissive =
                glm::mix(tf.material_points[index].material.emissive,
                         tf.material_points[index + 1].material.emissive, mix_factor);
        }

        // Store interpolated material properties in TF buffer
        tf_buffer[i * 4] = material.albedo[0];
        tf_buffer[i * 4 + 1] = material.albedo[1];
        tf_buffer[i * 4 + 2] = material.albedo[2];
        tf_buffer[(i + width) * 4] = material.metallicness;
        tf_buffer[(i + width) * 4 + 1] = material.emissive;
    }

    // Interpolate opacity values between TF control points
    for (uint32_t i = 0; i < width; ++i) {
        // Compute 1D texture coordinate (intensity)
        const float intensity = float(i) / (width - 1);

        // Interpolate opacity
        const uint32_t num_points = tf.opacity_points.size();
        float opacity = 0.0f;
        if (intensity <= tf.opacity_points[0].position) {
            opacity = tf.opacity_points[0].opacity;
        }
        else if (intensity >= tf.opacity_points[num_points - 1].position) {
            opacity = tf.opacity_points[num_points - 1].opacity;
        }
        else {
            uint32_t index = 0;
            for (uint32_t j = 0; j < num_points - 1; ++j) {
                if (intensity >= tf.opacity_points[j].position) {
                    index = j;
                }
            }

            const float mix_factor = linearstep(tf.opacity_points[index].position,
                                                tf.opacity_points[index + 1].position, intensity);
            opacity = glm::mix(tf.opacity_points[index].opacity,
                               tf.opacity_points[index + 1].opacity, mix_factor);
        }

        // Store interpolated opacity in TF buffer
        tf_buffer[i * 4 + 3] = opacity;
    }

    // Upload the transfer function buffer to a 2D texture
    tf.texture.target = GL_TEXTURE_2D;
    tf.texture.width = width;
    tf.texture.height = height;
    tf.texture.storage = {GL_RGBA16F, GL_RGBA, GL_FLOAT, 1};
    tf.texture.sampling = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE};
    gfx::texture_2d_create(tf.texture);
    gfx::texture_2d_upload_data(tf.texture, tf_buffer.data());
}

bool load_transfer_functions_from_json(const std::string &filename, const std::string &root_dir,
                                       std::vector<TransferFunction> &transfer_functions,
                                       std::vector<std::string> &transfer_function_ids)
{
    // Read JSON file
    std::ifstream json_file(filename);
    picojson::value root;
    json_file >> root;

    // Check for errors
    const std::string err = picojson::get_last_error();
    if (!err.empty()) {
        std::cerr << err << std::endl;
        return false;
    }
    if (!root.is<picojson::array>()) {
        return false;
    }

    // Parse JSON
    auto root_obj = root.get<picojson::array>();
    for (auto it = root_obj.begin(); it != root_obj.end(); it++) {
        if (!it->is<picojson::object>()) {
            continue;
        }
        auto tf_info = it->get<picojson::object>();

        // Fetch transfer function ID
        const std::string tf_id = tf_info.begin()->first;

        // Fetch transfer function filename
        if (!tf_info.begin()->second.is<std::string>()) {
            continue;
        }
        const std::string tf_filename = tf_info.begin()->second.get<std::string>();

        // Load transfer function
        TransferFunction tf;
        if (transfer_function_load(tf, root_dir + tf_filename)) {
            transfer_function_create_texture(tf);
            transfer_functions.push_back(tf);
            transfer_function_ids.push_back(tf_id);
        }
    }

    return true;
}

} // namespace ptvol
