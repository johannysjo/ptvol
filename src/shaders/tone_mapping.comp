#version 450

layout(local_size_x = 8, local_size_y = 8) in;

layout(binding = 0) uniform sampler2D u_color_texture;
layout(binding = 0, rgba8) uniform restrict writeonly image2D u_output_image;

layout(location = 0) uniform uint u_tone_mapping_op;
layout(location = 1) uniform float u_exposure;

#define TONE_MAPPING_OP_ACES 0
#define TONE_MAPPING_OP_UNREAL 1
#define TONE_MAPPING_OP_GAMMA_ONLY 2
#define TONE_MAPPING_OP_NONE 3

vec3 aces(vec3 x)
{
    float a = 2.51;
    float b = 0.03;
    float c = 2.43;
    float d = 0.59;
    float e = 0.14;
    return clamp((x * (a * x + b)) / (x * (c * x + d) + e), 0.0, 1.0);
}

vec3 unreal(vec3 linear_color)
{
    vec3 gamma_color = linear_color / (linear_color + 0.187) * 1.035;
    return gamma_color;
}

vec3 lin2srgb(vec3 color)
{
    return pow(color, vec3(0.454));
}

#if 0
vec3 linear2srgb(vec3 color)
{
    vec3 srgb_low = color * 12.92;
    vec3 srgb_high = (pow(abs(color), vec3(1.0 / 2.4)) * 1.055) - 0.055;
    vec3 srgb = vec3(0.0);
    srgb.r = (color.r <= 0.00031308) ? srgb_low.r : srgb_high.r;
    srgb.g = (color.g <= 0.00031308) ? srgb_low.g : srgb_high.g;
    srgb.b = (color.b <= 0.00031308) ? srgb_low.b : srgb_high.b;
    return srgb;
}
#endif

// Screen-space dither function from Alex Vlacho's "Advanced VR Rendering"
// presentation, GDC 2015.
vec3 screen_space_dither(vec2 screen_pos)
{
    vec3 dither = vec3(dot(vec2(171.0, 231.0), screen_pos));
    dither = fract(dither / vec3(103.0, 71.0, 97.0)) - 0.5;
	return (dither / 255.0) * 0.375;
}

void main()
{
    ivec2 imcoord = ivec2(gl_GlobalInvocationID.xy);

    vec4 output_color = texelFetch(u_color_texture, imcoord, 0);
    output_color.rgb *= exp(u_exposure);
    output_color.a = 1.0;

    // Apply tone mapping operator and gamma correction
    if (u_tone_mapping_op == TONE_MAPPING_OP_ACES) {
        output_color.rgb = aces(output_color.rgb);
        output_color.rgb = lin2srgb(output_color.rgb);
    }
    else if (u_tone_mapping_op == TONE_MAPPING_OP_UNREAL) {
        output_color.rgb = unreal(output_color.rgb);
    }
    else if (u_tone_mapping_op == TONE_MAPPING_OP_GAMMA_ONLY) {
        output_color.rgb = lin2srgb(output_color.rgb);
    }
    else if (u_tone_mapping_op == TONE_MAPPING_OP_NONE) {
        // do nothing
    }
    else { // invalid tone mapping operator
        output_color.rgb = vec3(1.0, 0.0, 1.0);
    }

    // Dither the result to reduce banding
    output_color.rgb += screen_space_dither(vec2(imcoord));

    imageStore(u_output_image, imcoord, output_color);
}
