/// @file
/// @brief Transfer function implementation and utilities.
///
/// @section LICENSE
///
/// Copyright (C) 2020  Johan Nysjö
///
/// This software is distributed under the MIT license. See the
/// included LICENSE.txt file for details.

#pragma once

#include "gfx.h"
#include <glm/vec3.hpp>

#include <string>
#include <vector>

namespace ptvol {

struct Material {
    glm::vec3 albedo;
    float metallicness;
    float gloss;
    float emissive;
};

struct MaterialTFPoint {
    float position;
    Material material;
};

struct OpacityTFPoint {
    float position;
    float opacity;
};

struct TransferFunction {
    std::vector<MaterialTFPoint> material_points;
    std::vector<OpacityTFPoint> opacity_points;
    gfx::Texture2D texture;
    bool is_dirty;
};

bool transfer_function_load(TransferFunction &tf, const std::string &filename);

void transfer_function_create_texture(TransferFunction &tf);

bool load_transfer_functions_from_json(const std::string &filename, const std::string &root_dir,
                                       std::vector<TransferFunction> &transfer_functions,
                                       std::vector<std::string> &transfer_function_ids);

} // namespace ptvol
