/// @file
/// @brief Utility functions.
///
/// @section LICENSE
///
/// Copyright (C) 2020  Johan Nysjö
///
/// This software is distributed under the MIT license. See the
/// included LICENSE.txt file for details.

#include "utils.h"
#include "gfx.h"

#include <glm/common.hpp>
#include <glm/exponential.hpp>
#include <glm/gtc/matrix_transform.hpp>
#define STBI_ONLY_PNG
#define STBI_ONLY_HDR
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#undef NDEBUG
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstdlib>

namespace ptvol {

GLuint load_shader_program(const std::string &compute_shader_filename)
{
    std::FILE *fp = std::fopen(compute_shader_filename.c_str(), "rb");
    assert(fp != nullptr);
    std::fseek(fp, 0, SEEK_END);
    std::string cshader_src;
    cshader_src.resize(std::ftell(fp));
    assert(cshader_src.size() > 0);
    std::rewind(fp);
    const size_t num_chars = std::fread(&cshader_src[0], 1, cshader_src.size(), fp);
    assert(num_chars == cshader_src.size());
    std::fclose(fp);

    const GLuint program = gfx::create_shader_program(cshader_src.c_str());

    return program;
}

void create_uint8_volume_texture(const std::vector<uint8_t> &voxel_data,
                                 const glm::uvec3 &dimensions, const GLuint min_filter,
                                 const GLuint mag_filter, const uint32_t max_level,
                                 gfx::Texture3D &texture)
{
    assert(voxel_data.size() == dimensions[0] * dimensions[1] * dimensions[2]);

    texture.target = GL_TEXTURE_3D;
    texture.width = dimensions[0];
    texture.height = dimensions[1];
    texture.depth = dimensions[2];
    texture.max_level = max_level;
    texture.storage = {GL_R8, GL_RED, GL_UNSIGNED_BYTE, 1};
    texture.sampling = {min_filter, mag_filter, GL_CLAMP_TO_BORDER};
    gfx::texture_3d_create(texture);
    assert(texture.texture != 0);

    gfx::texture_3d_upload_data(texture, voxel_data.data());
}

void load_rgba8_texture_array(const std::vector<std::string> &filenames,
                              gfx::Texture2DArray &texture)
{
    assert(filenames.size() > 0);

    const uint32_t num_layers = filenames.size();
    int32_t width, height, num_channels;
    const int32_t res = stbi_info(filenames[0].c_str(), &width, &height, &num_channels);
    assert(res != 0);

    texture.target = GL_TEXTURE_2D_ARRAY;
    texture.width = width;
    texture.height = height;
    texture.depth = num_layers;
    texture.storage = {GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE, 1};
    texture.sampling = {GL_LINEAR, GL_LINEAR, GL_REPEAT};
    gfx::texture_2d_array_create(texture);
    assert(texture.texture != 0);

    for (uint32_t i = 0; i < num_layers; ++i) {
        int32_t width_i, height_i, num_channels_i;
        uint8_t *data_i = stbi_load(filenames[i].c_str(), &width_i, &height_i, &num_channels_i, 4);
        assert(data_i != nullptr);
        assert(width_i == width);
        assert(height_i == height);

        gfx::texture_2d_array_upload_data(texture, i, data_i);

        stbi_image_free(data_i);
    }
}

void load_hdr_texture(const char *filename, gfx::Texture2D &texture)
{
    int32_t width, height, num_channels;
    float *data = stbi_loadf(filename, &width, &height, &num_channels, 4);
    assert(data != nullptr);

    texture.target = GL_TEXTURE_2D;
    texture.width = width;
    texture.height = height;
    texture.storage = {GL_RGBA16F, GL_RGBA, GL_FLOAT, 1};
    texture.sampling = {GL_LINEAR, GL_LINEAR, GL_REPEAT};
    gfx::texture_2d_create(texture);
    assert(texture.texture != 0);
    gfx::texture_2d_upload_data(texture, data);

    stbi_image_free(data);
}

float compute_exposure(const CameraExposureInfo &exposure_info)
{
    const float ev100 = glm::log2(glm::pow(exposure_info.aperture, 2.0f) /
                                  exposure_info.shutter_time * 100.0f / exposure_info.iso);
    const float max_luminance = 1.2f * glm::pow(2.0f, ev100);

    return 1.0f / max_luminance;
}

void fit_frustum_to_bsphere(const float bsphere_radius, const glm::vec3 &bsphere_center,
                            Camera &camera)
{
    const float theta = 0.5f * camera.fovy;
    const float eye_to_center_dist = camera.z_near + bsphere_radius / std::sin(theta);
    const glm::vec3 view_dir = glm::normalize(camera.center - camera.eye);

    camera.eye = bsphere_center - view_dir * eye_to_center_dist;
    camera.center = bsphere_center;
    camera.z_far = eye_to_center_dist + 1.1f * bsphere_radius;
}

void pan_controller_start(PanController &pan_controller, const glm::vec2 &point)
{
    pan_controller.origin = point;
    pan_controller.panning = true;
}

void pan_controller_pan(PanController &pan_controller, const glm::vec2 &point, const Camera &camera,
                        const glm::ivec4 &viewport)
{
    const glm::mat4 view_matrix = glm::lookAt(camera.eye, camera.center, camera.up);
    const glm::mat4 projection_matrix =
        glm::perspective(camera.fovy, camera.aspect, camera.z_near, camera.z_far);

    // Convert current mouse position to world coordinates
    const glm::vec3 sc = {point.x, viewport.w - point.y, camera.z_near};
    const glm::vec3 wc = glm::unProject(sc, view_matrix, projection_matrix, viewport);

    // Convert previous mouse position to world coordinates
    const glm::vec3 sc_origin = {pan_controller.origin.x, viewport.w - pan_controller.origin.y,
                                 camera.z_near};
    const glm::vec3 wc_origin = glm::unProject(sc_origin, view_matrix, projection_matrix, viewport);

    // Update pan translation and origin
    const glm::vec3 wc_diff = wc - wc_origin;
    pan_controller.translation += 0.5f * glm::vec3(wc_diff.x, wc_diff.y, 0.0f);
    pan_controller.origin = glm::vec2(point);
}

void pan_controller_stop(PanController &pan_controller)
{
    pan_controller.panning = false;
}

void pan_controller_reset(PanController &pan_controller)
{
    pan_controller.origin = glm::ivec2(0, 0);
    pan_controller.translation = glm::vec3(0.0f, 0.0f, 0.0f);
    pan_controller.panning = false;
}

} // namespace ptvol
