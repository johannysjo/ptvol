#pragma once

#include <stdint.h>
#include <string>

namespace ptvol {

struct FileDialogState {
    std::string current_dir;
    std::string selected_filename;
};

std::string file_dialog_open(FileDialogState &state);

} // namespace ptvol
