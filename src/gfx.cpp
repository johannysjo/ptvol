#include "gfx.h"

#undef NDEBUG
#include <cassert>
#include <cstdio>
#include <cstdlib>

namespace {

void show_shader_info_log(const GLuint shader)
{
    GLint info_log_length = 0;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_log_length);
    if (info_log_length > 1) {
        char *info_log = (char *)std::malloc(info_log_length * sizeof(char));
        glGetShaderInfoLog(shader, info_log_length, nullptr, info_log);
        std::printf("%s\n", info_log);
        std::free(info_log);
    }
}

void show_program_info_log(const GLuint program)
{
    GLint info_log_length = 0;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &info_log_length);
    if (info_log_length > 1) {
        char *info_log = (char *)std::malloc(info_log_length * sizeof(char));
        glGetProgramInfoLog(program, info_log_length, nullptr, info_log);
        std::printf("%s\n", info_log);
        std::free(info_log);
    }
}

} // namespace

namespace gfx {

void texture_2d_create(Texture2D &texture)
{
    assert(texture.target == GL_TEXTURE_2D);

    glCreateTextures(texture.target, 1, &texture.texture);

    glTextureParameteri(texture.texture, GL_TEXTURE_MIN_FILTER, texture.sampling.min_filter);
    glTextureParameteri(texture.texture, GL_TEXTURE_MAG_FILTER, texture.sampling.mag_filter);
    glTextureParameteri(texture.texture, GL_TEXTURE_WRAP_S, texture.sampling.wrap);
    glTextureParameteri(texture.texture, GL_TEXTURE_WRAP_T, texture.sampling.wrap);

    glTextureStorage2D(texture.texture, 1, texture.storage.internal_format, texture.width,
                       texture.height);
}

void texture_2d_upload_data(Texture2D &texture, const void *data)
{
    assert(texture.texture != 0);

    GLint unpack_alignment_old;
    glGetIntegerv(GL_UNPACK_ALIGNMENT, &unpack_alignment_old);
    glPixelStorei(GL_UNPACK_ALIGNMENT, texture.storage.unpack_alignment);

    glTextureSubImage2D(texture.texture, 0, 0, 0, texture.width, texture.height,
                        texture.storage.format, texture.storage.type, data);

    glPixelStorei(GL_UNPACK_ALIGNMENT, unpack_alignment_old);
}

void texture_2d_resize(Texture2D &texture, const uint32_t width, const uint32_t height)
{
    texture.width = width;
    texture.height = height;
    glDeleteTextures(1, &texture.texture);
    texture_2d_create(texture);
}

void texture_2d_array_create(Texture2DArray &texture)
{
    assert(texture.target == GL_TEXTURE_2D_ARRAY);

    glCreateTextures(texture.target, 1, &texture.texture);

    glTextureParameteri(texture.texture, GL_TEXTURE_MIN_FILTER, texture.sampling.min_filter);
    glTextureParameteri(texture.texture, GL_TEXTURE_MAG_FILTER, texture.sampling.mag_filter);
    glTextureParameteri(texture.texture, GL_TEXTURE_WRAP_S, texture.sampling.wrap);
    glTextureParameteri(texture.texture, GL_TEXTURE_WRAP_T, texture.sampling.wrap);

    glTextureStorage3D(texture.texture, 1, texture.storage.internal_format, texture.width,
                       texture.height, texture.depth);
}

void texture_2d_array_upload_data(Texture2DArray &texture, const uint32_t layer, const void *data)
{
    assert(texture.texture != 0);
    assert(layer < texture.depth);

    GLint unpack_alignment_old;
    glGetIntegerv(GL_UNPACK_ALIGNMENT, &unpack_alignment_old);
    glPixelStorei(GL_UNPACK_ALIGNMENT, texture.storage.unpack_alignment);

    glTextureSubImage3D(texture.texture, 0, 0, 0, layer, texture.width, texture.height, 1,
                        texture.storage.format, texture.storage.type, data);

    glPixelStorei(GL_UNPACK_ALIGNMENT, unpack_alignment_old);
}

void texture_3d_create(Texture3D &texture)
{
    assert(texture.target == GL_TEXTURE_3D);

    glCreateTextures(texture.target, 1, &texture.texture);

    glTextureParameteri(texture.texture, GL_TEXTURE_BASE_LEVEL, 0);
    glTextureParameteri(texture.texture, GL_TEXTURE_MAX_LEVEL, texture.max_level);
    glTextureParameteri(texture.texture, GL_TEXTURE_MIN_FILTER, texture.sampling.min_filter);
    glTextureParameteri(texture.texture, GL_TEXTURE_MAG_FILTER, texture.sampling.mag_filter);
    glTextureParameteri(texture.texture, GL_TEXTURE_WRAP_S, texture.sampling.wrap);
    glTextureParameteri(texture.texture, GL_TEXTURE_WRAP_T, texture.sampling.wrap);
    glTextureParameteri(texture.texture, GL_TEXTURE_WRAP_R, texture.sampling.wrap);

    glTextureStorage3D(texture.texture, texture.max_level + 1, texture.storage.internal_format,
                       texture.width, texture.height, texture.depth);
}

void texture_3d_upload_data(Texture3D &texture, const void *data)
{
    assert(texture.texture != 0);

    GLint unpack_alignment_old;
    glGetIntegerv(GL_UNPACK_ALIGNMENT, &unpack_alignment_old);
    glPixelStorei(GL_UNPACK_ALIGNMENT, texture.storage.unpack_alignment);

    glTextureSubImage3D(texture.texture, 0, 0, 0, 0, texture.width, texture.height, texture.depth,
                        texture.storage.format, texture.storage.type, data);

    glPixelStorei(GL_UNPACK_ALIGNMENT, unpack_alignment_old);
}

void texture_3d_upload_mipmap_level(Texture3D &texture, const uint32_t level, const uint32_t width,
                                    const uint32_t height, const uint32_t depth, const void *data)
{
    assert(texture.texture != 0);

    GLint unpack_alignment_old;
    glGetIntegerv(GL_UNPACK_ALIGNMENT, &unpack_alignment_old);
    glPixelStorei(GL_UNPACK_ALIGNMENT, texture.storage.unpack_alignment);

    glTextureSubImage3D(texture.texture, level, 0, 0, 0, width, height, depth,
                        texture.storage.format, texture.storage.type, data);

    glPixelStorei(GL_UNPACK_ALIGNMENT, unpack_alignment_old);
}

void fbo_create(FBO &fbo, const uint32_t width, const uint32_t height, const bool resizeable)
{
    glCreateFramebuffers(1, &fbo.fbo);
    assert(fbo.fbo != 0);
    fbo.width = width;
    fbo.height = height;
    fbo.resizeable = resizeable;
}

void fbo_attach_texture(FBO &fbo, const GLenum attachment, Texture2D texture)
{
    assert(fbo.fbo != 0);
    assert(fbo.width == texture.width);
    assert(fbo.height == texture.height);

    fbo.attachments[attachment] = texture;
    glNamedFramebufferTexture(fbo.fbo, attachment, texture.texture, 0);
}

bool fbo_is_complete(const FBO &fbo)
{
    const GLenum status = glCheckNamedFramebufferStatus(fbo.fbo, GL_FRAMEBUFFER);
    return status == GL_FRAMEBUFFER_COMPLETE;
}

void fbo_resize(FBO &fbo, const uint32_t width, const uint32_t height)
{
    assert(fbo.fbo != 0);

    if (!fbo.resizeable) {
        return;
    }

    fbo.width = width;
    fbo.height = height;
    for (auto &it : fbo.attachments) {
        texture_2d_resize(it.second, width, height);
        glNamedFramebufferTexture(fbo.fbo, it.first, it.second.texture, 0);
    }
}

GLuint create_shader_program(const char *vertex_shader_src, const char *fragment_shader_src)
{
    GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    {
        glShaderSource(vertex_shader, 1, &vertex_shader_src, nullptr);
        glCompileShader(vertex_shader);
        GLint compiled;
        glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &compiled);
        if (!compiled) {
            std::printf("Vertex shader compilation failed:\n");
            show_shader_info_log(vertex_shader);
            glDeleteShader(vertex_shader);
            return 0;
        }
    }

    GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    {
        glShaderSource(fragment_shader, 1, &fragment_shader_src, nullptr);
        glCompileShader(fragment_shader);
        GLint compiled;
        glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &compiled);
        if (!compiled) {
            std::printf("Fragment shader compilation failed:\n");
            show_shader_info_log(fragment_shader);
            glDeleteShader(vertex_shader);
            glDeleteShader(fragment_shader);
            return 0;
        }
    }

    GLuint program = glCreateProgram();
    {
        glAttachShader(program, vertex_shader);
        glAttachShader(program, fragment_shader);

        glLinkProgram(program);
        GLint linked;
        glGetProgramiv(program, GL_LINK_STATUS, &linked);
        if (!linked) {
            std::printf("Program linking failed:\n");
            show_program_info_log(program);
            glDeleteProgram(program);
            glDeleteShader(vertex_shader);
            glDeleteShader(fragment_shader);
            return 0;
        }

        glDetachShader(program, vertex_shader);
        glDetachShader(program, fragment_shader);
    }

    return program;
}

GLuint create_shader_program(const char *compute_shader_src)
{
    GLuint compute_shader = glCreateShader(GL_COMPUTE_SHADER);
    glShaderSource(compute_shader, 1, &compute_shader_src, nullptr);
    glCompileShader(compute_shader);
    GLint compiled;
    glGetShaderiv(compute_shader, GL_COMPILE_STATUS, &compiled);
    if (!compiled) {
        std::printf("Compute shader compilation failed:\n");
        show_shader_info_log(compute_shader);
        glDeleteShader(compute_shader);
        return 0;
    }

    GLuint program = glCreateProgram();
    glAttachShader(program, compute_shader);
    glLinkProgram(program);
    GLint linked;
    glGetProgramiv(program, GL_LINK_STATUS, &linked);
    if (!linked) {
        std::printf("Program linking failed:\n");
        show_program_info_log(program);
        glDeleteProgram(program);
        glDeleteShader(compute_shader);
        return 0;
    }
    glDetachShader(program, compute_shader);

    return program;
}

} // namespace gfx
