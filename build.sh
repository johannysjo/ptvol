#!/bin/bash

export PTVOL_ROOT=$(pwd)

# Generate build script
cd $PTVOL_ROOT && \
if [ ! -d build ]; then
    mkdir build
fi
cd build && \
cmake -DCMAKE_INSTALL_PREFIX=$PTVOL_ROOT ../ && \

# Build and install the program
make -j4 && \
make install && \

# Run the program
cd ../bin && \
./ptvol
