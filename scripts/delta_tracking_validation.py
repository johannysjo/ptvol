import math
import random


def delta_tracking(scattering, absorption, path_length):
    sigma_t = scattering + absorption
    max_density = 1.0
    inv_max_density = 1.0 / max_density

    collision = False
    t = 0.0
    while not collision:
        t -= math.log(max(1e-9, random.random())) * inv_max_density / sigma_t
        if t > path_length:
            break

        density = max_density
        collision = density * inv_max_density > random.random()

    return collision


def beer_lambert(scattering, absorption, path_length):
    optical_thickness = (scattering + absorption) * path_length
    transmittance = math.exp(-optical_thickness)

    return transmittance


def main():
    scattering = 0.0
    absorption = 0.1
    path_length = 0.5

    num_iterations = 100000
    transmittance_approx = 0.0
    for i in xrange(0, num_iterations):
        collision = delta_tracking(scattering, absorption, path_length)
        if not collision:
            transmittance_approx += 1.0
    transmittance_approx /= num_iterations

    transmittance_analytic = beer_lambert(scattering, absorption, path_length)

    print(transmittance_approx)
    print(transmittance_analytic)


if __name__ == "__main__":
    main()
