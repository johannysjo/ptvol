"""
.. module:: int16_to_uint8
   :platform: Unix
   :synopsis: VTK volume conversion script.

.. moduleauthor:: Johan Nysjo

Converts an int16 VTK volume to uint8 format.

"""

import argparse

import volume


def main():
    # Parse command-line arguments
    parser = argparse.ArgumentParser(prog="int16_to_uint8")
    parser.add_argument(
        "volume_int16", help="name of VTK volume to load")
    parser.add_argument(
        "output", nargs="?", help="output filename")
    args = parser.parse_args()

    volume_int16_filename = args.volume_int16
    if args.output:
        output_filename = args.output
    else:
        output_filename = "output.vtk"

    # Load and convert volume
    volume_int16 = volume.load_vtk_volume(volume_int16_filename)
    volume_uint8 = volume.int16_to_uint8(volume_int16)
    volume.save_vtk_volume(volume_uint8, output_filename)


if __name__ == "__main__":
    main()
