# PTVol
PTVol is an interactive volumetric GPU path tracer that uses delta (Woodcock) tracking to produce highly realistic visualizations of heterogeneous volume data, for instance, anatomical structures in computed tomography (CT) images. This is the same approach as was described by Thomas Kroes et al. in the paper "Exposure Render: An Interactive Photo-Realistic Volume Rendering Framework", PLOS ONE, 2012. The basic idea of delta tracking is to fill the heterogeneous volume with fictious particles to a homegeneous volume, sample it with random step lengths based on the mean free-path distance that a photon will travel in the homogeneous volume until it hits a particle, and randomly classify each sample as a real or false particle collision based on the local density in the heterogeneous volume. The tracking then continues in the same direction as before if there was no collision, or in a new direction if there was a real collision and a scattering event.

The renderer is implemented in C++ using OpenGL 4.5 and GLSL compute shaders, and supports the following features:

- GPU-accelerated delta tracking with progressive refinement
- Empty-space skipping
- Low-discrepancy sampling with digit-permuted Halton sequences. A per-pixel shift (Cranley-Patterson rotation with blue noise) is applied on the sequences to randomize them between pixels.
- Sampling of equirectangular HDR environment maps (.hdr images)
- Surface and volumetric scattering
- A simple denoiser based on bilateral filtering and temporal anti-aliasing (TAA)
- Color and opacity transfer function editor, with presets for skin, soft tissue, and bone
- Camera-aligned clipping plane
- Filmic tone mapping and bloom

A max intensity block volume is used for both empty-space skipping and for estimating the maximum extinction coefficient (majorant) along the ray, which speeds up the tracking.

## Example screenshot
![Screenshot chameleon](https://bitbucket.org/johannysjo/ptvol/raw/master/resources/screenshot_ptvol_chameleon.png "Screenshot")

Visualization of the Chameleon CT volume dataset from the [Digital Morphology Library](http://digimorph.org/index.phtml) (DigiMorph), UT Austin.

## Compiling (Linux)
Clone the git repository and run (from the root folder)
```bash
$ ./build.sh
```
Requires CMake 3.1 or higher and a C++11 capable compiler (GCC 4.6+).

## Usage
Set the environment variable
```bash
$ export PTVOL_ROOT=/path/to/ptvol/directory
```
and run the program as
```bash
$ bin/ptvol data/manix.vtk
```
Currently, the renderer can only import volume image files in the legacy VTK uint8 and int16 formats. It also supports loading of HDR environment maps (32-bit .hdr images), which can be downloaded from [here](https://polyhaven.com/hdris). Volumes and HDR environment maps can be loaded into the program from the file menu or with drag and drop.

## License
PTVol is provided under the MIT license. See LICENSE.txt for more information.

## More screenshots
Visualization of the MANIX dataset from the OsiriX DICOM image library:

![Screenshot manix](https://bitbucket.org/johannysjo/ptvol/raw/master/resources/screenshot_ptvol_manix.png)

Visualization of the OBELIX dataset from the OsiriX DICOM image library:

![Screenshot obelix](https://bitbucket.org/johannysjo/ptvol/raw/master/resources/screenshot_ptvol_obelix.png)

Camera-aligned clipping planes:

![Screenshot clip planes](https://bitbucket.org/johannysjo/ptvol/raw/master/resources/screenshot_ptvol_clip_planes.png)

The Chameleon dataset, rendered with different HDR environment maps:

![Screenshot envmaps](https://bitbucket.org/johannysjo/ptvol/raw/master/resources/screenshot_ptvol_chameleon_envmaps.png)

Progressive refinement, with and without denoising:

![Screenshot denoising](https://bitbucket.org/johannysjo/ptvol/raw/master/resources/screenshot_ptvol_denoising.png)

ImGui user interface:

![Screenshot UI](https://bitbucket.org/johannysjo/ptvol/raw/master/resources/screenshot_ptvol_ui.png)
